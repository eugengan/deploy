
#!/bin/sh

rm -rf /home/devops/Projects/repo/frontend
cd /home/devops/Projects/repo/
#git clone https://Yuryn:'9hXCh10axz79h!&'@bitbucket.org/Yuryn/frontend.git -b development
git clone git@bitbucket.org:Yuryn/frontend.git  -b development
cp /home/devops/Projects/api.nginx.conf /home/devops/Projects/repo/frontend/api.conf
cp /home/devops/Projects/ssl-params.conf /home/devops/Projects/repo/frontend/ssl-params.conf
cp /home/devops/Projects/Dockerfile.front /home/devops/Projects/repo/frontend/Dockerfile
 
cp /home/devops/Projects/frontend.env /home/devops/Projects/repo/frontend/.env
cp /home/devops/Projects/front.docker-compose.yml /home/devops/Projects/repo/frontend/docker-compose.yml
cd /home/devops/Projects/repo/frontend

docker-compose up --build -d
