  GNU nano 2.9.3                                                                                  deploy_back.sh                                                                                            

#!/bin/sh
rm -rf /home/devops/Projects/repo/backend
rm -rf /home/devops/Projects/repo/environment
cd /home/devops/Projects/repo/

git clone git@bitbucket.org:Yuryn/backend.git -b development

cp /home/devops/Projects/backend.env  /home/devops/Projects/repo/backend/.env
cp /home/devops/Projects/backend.env  /home/devops/Projects/repo/backend/docker/.env

cd /home/devops/Projects/repo/backend
docker-compose build
docker-compose down && docker-compose up -d
