#!/bin/sh

rm -rf /home/devops/project/repo/frontend
cd /home/devops/project/repo/

git clone git@bitbucket.org:iqubate/taxlocator.git frontend

cp /home/devops/project/front.nginx.conf /home/devops/project/repo/frontend/api.conf
cp /home/devops/project/Dockerfile.front /home/devops/project/repo/frontend/Dockerfile

cp /home/devops/project/frontend.env /home/devops/project/repo/frontend/.env
cp /home/devops/project/front.docker-compose.yml /home/devops/project/repo/frontend/docker-compose.yml
cd /home/devops/project/repo/frontend

docker-compose up --build -d
