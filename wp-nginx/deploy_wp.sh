#!/bin/sh
sudo chmod -R 777 /home/devops/Projects/repo/wp
#rm -rf /home/devops/Projects/repo/wp
cd /home/devops/Projects/repo/
#git clone https://Yuryn:'9hXCh10axz79h!&'@bitbucket.org/Yuryn/wp.git . -b development
cp /home/devops/Projects/wp.docker-compose.yml /home/devops/Projects/repo/wp/docker-compose.yml
cp /home/devops/Projects/Dockerfile.wp /home/devops/Projects/repo/wp/Dockerfile
cp /home/devops/Projects/wp.nginx.conf /home/devops/Projects/repo/wp/api.conf
cp /home/devops/Projects/ssl-params.conf /home/devops/Projects/repo/wp/ssl-params.conf
cp /home/devops/Projects/php.ini /home/devops/Projects/repo/wp/php.ini
cp /home/devops/Projects/robot.txt /home/devops/Projects/repo/wp/robot.txt

cd /home/devops/Projects/repo/wp
ls
docker-compose build
docker-compose down && docker-compose up -d
docker exec wordpress chown -R www-data:www-data /var/www/html/wp-content
docker exec wordpress chmod 755 /var/www/html/wp-content

